<?php

namespace App\Http\Controllers;

use App\DoctorNotes;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    public function getNotes(Request $request)
    {
        # code...
        $notes = DoctorNotes::all();
        return response()->json([
            'notes' => $notes
        ], true);
    }
}

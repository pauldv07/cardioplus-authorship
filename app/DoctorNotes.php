<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorNotes extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'notes'
    ];

    protected $table = 'DoctorNotes';

    
}
